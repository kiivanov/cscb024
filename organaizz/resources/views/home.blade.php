@extends('layouts.app')

@section('title', 'events')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">           
            <div>
                <h5 style="margin-left: 42%;">Public events for:</h5>
            </div>
            <div class="col-md-12">
                <div id="calendar"></div>
            </div>

            <div class="row justify-content-center mb-3">
                <div style="text-align: center; padding-top: 20px;">
                    <div class="text-center">
                    </div>
                    @if (auth()->user() && auth()->user()->organizer == false)
                    <div class="float-md-none">
                        <a href="{{ route('event.create') }}" class="btn btn-lg btn-primary">Create event</a>
                    </div>
                    @endif
                </div>
            </div>

        </div>
    </div>
    <script>
        var eventArray = @json($eventArray, JSON_PRETTY_PRINT);
    </script>
    @endsection