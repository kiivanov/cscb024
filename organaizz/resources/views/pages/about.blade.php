@extends('layouts.app')

@section('title', 'About')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-10">
                    <div class="p-3 mb-2">
                        <h3><i>Headline</i></h3>
                    </div>

                    <div>
                        Event organizer project for CSCB024 developed by:....
                        <div class="p-3 lead">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection