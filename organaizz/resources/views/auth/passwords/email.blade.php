@extends('layouts.app')

@section('title', 'Event Organizer')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="text-center pt-3 pb-2 mb-3">
                            <h1>{{ __('Forgot your password ?') }}</h1>
                        </div>

                        <div class="text-center col-md-6 mb-3 offset-md-3 mb-3">
                            Password reset instructions will be sent over email.
                        </div>

                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-md-6 mb-3 offset-md-3">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Enter you account email." value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-lg btn-primary">
                                    {{ __('Send Instructions') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection